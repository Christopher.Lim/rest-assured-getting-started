package com.rest;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;
import org.testng.annotations.Test;

public class StaticImports {

    @Test
    public void simple_test_case() {
        given()
        .baseUri("https://api.postman.com")
        .header("x-api-key", "PMAK-607f914cb7114b004356b032-d307637aba5ca60eff96fcfb013a732ff2")
        .when()
        .get("/workspaces")
        .then()
        .statusCode(200)
        .body("name",is(equalTo(null)),
                "type", is(equalTo(null)));

    }
}
